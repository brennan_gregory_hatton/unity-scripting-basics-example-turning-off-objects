﻿/*
Example script of how to turn off lights, triggered by pressing space
by Brennan Hatton - siliconvagabond.com 

Send any questions to brennan@siliconvagabond.com
*/

using UnityEngine;
using System.Collections;

public class TurnOnLights : MonoBehaviour {
	
	//You can make this more efficent by replacing all of these with the bellow commented out line
	//public List<GameObject> lights = new List<GameObject>(); //If you use this line, instead of using "light1, light2, light3.. etc" you can use "lights[0], lights[1], lights[2]" and treat them like an array.
	
	
	//List the objects you want to turn off here 
	public GameObject light1;
	public GameObject light2;
	public GameObject light3;
	
	//variable for number of times spacebar is pressed
	int spacePressed = 0;
	
	
	// Update is called once per frame
	void Update () {
		
		//Checks for space being pressed down (only gets triggered once when spac eis pressed)
		if (Input.GetKeyDown(KeyCode.Space))
		{
			//increase spacePressed by one
			spacePressed += 1;
			
			//Turn on lights
			switch(spacePressed)
			{
			case 1:
				
				light1.SetActive(true);
				break;
				
			case 2:
				
				light2.SetActive(true);
				light1.SetActive(false);
				break;
				
			case 3:
				
				light3.SetActive(true);
				break;
				
			default:
				
				light1.SetActive(true);
				light2.SetActive(true);
				light3.SetActive(true);
				break;
				
			}
		}
	}
}
