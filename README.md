# README #

This project is an example of how to turn off lights with scripting in Unity. Built while live on skype with Josh and Ender for projects.invisionapp.com/boards/5Q10MWXHJFZDV

Built with Unity5.3 & ScriptInspector

There are optimizations, commented in TurnOffLights.cs

For questions contract brennan@siliconvagabond.com